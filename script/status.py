# Script 02: Panel data on the status of FVG enterprises

# Authors: Andrea Bincoletto, Leyla Vesnic @Area Science Park
# Description: python script to clean and prepare historical data of FVG capital companies (2019-2022)
# Source: Innovation Intelligence FVG (I2FVG)


# Setup
import sys
import os
from pathlib import Path
# %matplotlib inline
import matplotlib as plt
from matplotlib import pyplot as plt
import datetime
import pandas as pd
import numpy as np
import glob
pd.options.display.max_columns = None

# directory "input"
current_path = Path(os.getcwd())
data_subdir = "input\\status impresa"
data_path = current_path / data_subdir
data_dir = str(data_path)

# lettura di tutti i file con estensione .xlsx
filenames = glob.glob(data_dir + "\*.xlsx")
cols_to_use = ['c fiscale', 
            "UL-SEDE - Unità Locale o sede dell'impresa", 
            'stato impresa/ul']

# assegnare i nomi ai dataset
list_of_names = ['.\\FRIULI anagrafica_2019','.\\FRIULI anagrafica_2020', '.\\FRIULI anagrafica_2021','.\\FRIULI anagrafica_2022']
 
# creare una lista vuota
dataframes_list = []
 
# append di tutti i dataset alla lista vuota
for i in range(len(list_of_names)):
    # temp_df = pd.read_excel(data_dir+list_of_names[i]+".xlsx", header=0, dtype={'c fiscale': 'str'}, keep_default_na=True, usecols=cols_to_use)
    temp_df = pd.read_excel(data_dir+list_of_names[i]+".xlsx", header=0, dtype=str, keep_default_na=True, usecols=cols_to_use)
    dataframes_list.append(temp_df)

# creare una colonna con l'anno di riferimento
year = 2018     # to modify if first original file has changed
for i in range(len(dataframes_list)):
    df = dataframes_list[i]
    dataframes_list[i] = df[df["UL-SEDE - Unità Locale o sede dell'impresa"].isin(['SEDE'])]
    year = year + 1
    dataframes_list[i]['year_reference'] = year

# rinomino i dataframe nella lista con il riferimento all'anno
df_2019 = dataframes_list[0]
df_2020 = dataframes_list[1]
df_2021 = dataframes_list[2]
df_2022 = dataframes_list[3]

# merge dei dataframe inclusi nella lista
dframe = [df_2019, df_2020, df_2021, df_2022]
result = pd.concat([dframe[i] for i in range(len(dframe))], axis=0)
result = result[['c fiscale', 'stato impresa/ul', 'year_reference']]
result = result.sort_values(['c fiscale', 'year_reference'], ascending=[True, True])

# Saving in csv format
current_path = Path(os.getcwd())
data_subdir = "output"
data_path = current_path.parent / data_subdir
data_dir = str(data_path)
output_file = data_dir + '\\' + 'status_storico.csv'
result.to_csv(      output_file, 
                    sep ='|',   
                    encoding='utf-8-sig', 
                    index=False
                    )