# Script 03: Panel data on network contracts of FVG enterprises

# Authors: Andrea Bincoletto, Leyla Vesnic @Area Science Park
# Description: python script to clean and prepare historical data of FVG capital companies (2019-2022)
# Source: Innovation Intelligence FVG (I2FVG)


# Setup
import sys
import os
from pathlib import Path
# %matplotlib inline
import matplotlib as plt
from matplotlib import pyplot as plt
import datetime
import pandas as pd
import numpy as np
import glob
pd.options.display.max_columns = None

# directory "input"
current_path = Path(os.getcwd())
data_subdir = "input//contratti di rete"
data_path = current_path / data_subdir
data_dir = str(data_path)
cols_to_use = ['denominazione contratto', 
            "data atto",
            'numero repertorio'
            ]
anno_contratto = pd.read_excel(data_dir+"\\ContrattiRete_4aprile2022_mod.xlsx", sheet_name="Elenco", header=0, dtype=str, keep_default_na=True, usecols=cols_to_use)
anno_contratto['data atto'] = pd.to_datetime(anno_contratto['data atto'])

# drop degli stessi contratti
anno_contratto.drop_duplicates(subset='denominazione contratto', inplace = True)

# Handling rows with the name 'ASSENTE' (with a different name from I2FVG)

# unione nella stessa colonna di ASSENTE+numero repertorio (come in I2FVG)
df_assenti = anno_contratto[anno_contratto['denominazione contratto'].isin(['ASSENTE'])]

for i in range(len(df_assenti)):
    df_assenti['denominazione contratto'] = df_assenti['denominazione contratto'] + '-' + df_assenti['numero repertorio']
    break

# unione df_assenti con le altre denominazioni contratti (prima si eliminano gli assenti dal df originale)
contract_to_drop = ['ASSENTE']
anno_contratto = anno_contratto.loc[~anno_contratto['denominazione contratto'].isin(contract_to_drop)]
anno_contratto = anno_contratto.append(df_assenti)
anno_contratto.sort_values(by = ['denominazione contratto', 'data atto'], inplace=True, ascending=[True, True])

# Handling duplicate rows

# df delle righe duplicate
df_duplicati = anno_contratto.loc[anno_contratto.duplicated(subset='denominazione contratto', keep=False)]
df_duplicati = df_duplicati.drop_duplicates(['denominazione contratto'], keep='last')

# append delle righe duplicate sistemate in df_duplicati (prima rimuovo quelle righe da anno_contratti)
contract_to_drop = ['CONTRATTO DI RETE',
                    'CONVENTION BUREAU NAPOLI',
                    'FRIULI WEDDING NETWORK',
                    'ORGANIC INTEGRITY PLATFORM (OIP)',
                    'RETE ITALIANA OPEN SOURCE (ABBREVIATO RIOS)',
                    'RETE MARINE FRIULI VENEZIA GIULIA'
                    ]
anno_contratto = anno_contratto.loc[~anno_contratto['denominazione contratto'].isin(contract_to_drop)]
anno_contratto = anno_contratto.append(df_duplicati)

# List of fiscal codes and contracts from I2FVG extraction (2022)

# lettura csv file da I2FVG (2022)
cols_to_use = ['A-Denominazione', 
            "A-Comune SEDE", 
            'A-Codice fiscale',
            'A-Partita IVA',
            'C-Contratti di rete denominazione',
            'C-Contratti di rete oggetto',
            'C-Imprese partecipanti al contratto di rete',
            'C-Contratti di rete soggetto giuridico (SI/NO)'
            ]
df_cf_contratti = pd.read_csv(data_dir+"\\contrattirete_I2FVG_2022.csv", sep = ',', encoding='utf-8-sig', dtype=str, usecols=cols_to_use)
contract_to_drop = [' NESSUN CONTRATTO']
df_cf_contratti = df_cf_contratti.loc[~df_cf_contratti['C-Contratti di rete denominazione'].isin(contract_to_drop)]
anno_contratto.rename(columns={'denominazione contratto':'C-Contratti di rete denominazione'}, inplace=True)
anno_contratto = anno_contratto[['C-Contratti di rete denominazione', 'data atto']]

# unione dei codici fiscali (da I2FVG, df_cf_contratti) and il df dei nomi dei contratti (anno_contratto)
df_final = pd.merge(left=df_cf_contratti, right=anno_contratto, on='C-Contratti di rete denominazione', how='left')

# Saving in csv format
current_path = Path(os.getcwd())
data_subdir = "output"
data_path = current_path.parent / data_subdir
data_dir = str(data_path)
output_file = data_dir + '\\' + 'contrattirete_storico.csv'
df_final.to_csv(    output_file, 
                    sep ='|',   
                    encoding='utf-8-sig', 
                    index=False
                    )