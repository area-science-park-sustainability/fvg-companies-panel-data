# Script 04: Panel data on the number of employees of FVG enterprises

# Authors: Andrea Bincoletto, Leyla Vesnic @Area Science Park
# Description: python script to clean and prepare historical data of FVG capital companies (2019-2022)
# Source: Innovation Intelligence FVG (I2FVG)


# Setup
import sys
import os
from pathlib import Path
# %matplotlib inline
import matplotlib as plt
from matplotlib import pyplot as plt
import datetime
import pandas as pd
import numpy as np
import glob
pd.options.display.max_columns = None

# directory "input"
current_path = Path(os.getcwd())
data_subdir = "input//addetti"
data_path = current_path / data_subdir
data_dir = str(data_path)

# lettura di tutti i file con estensione .xlsx
filenames = glob.glob(data_dir + "\*.xlsx")
cols_to_use = ['c fiscale', 
            'PRV - Provincia', 
            'rea', 
            "UL-SEDE - Unità Locale o sede dell'impresa", 
            'tipo impresa', 
            'AA-ADD - Anno di dichiarazione degli addetti',
            'IND - Numero addetti indipendenti',
            'DIP - Numero addetti dipendenti',
            'stato impresa/ul']

# assegno nome ai dataset
list_of_names = ['.\\FRIULI anagrafica_2018','.\\FRIULI anagrafica_2019','.\\FRIULI anagrafica_2020', '.\\FRIULI anagrafica_2021','.\\FRIULI anagrafica_2022']
 
# lista vuota
dataframes_list = []
 
# append dei dataset nella lista vuota
for i in range(len(list_of_names)):
    # temp_df = pd.read_excel(data_dir+list_of_names[i]+".xlsx", header=0, dtype={'c fiscale': 'str'}, keep_default_na=True, usecols=cols_to_use)
    temp_df = pd.read_excel(data_dir+list_of_names[i]+".xlsx", header=0, dtype=str, keep_default_na=True, usecols=cols_to_use)
    dataframes_list.append(temp_df)

# filtrare i dati per SEDE e modifica del tipo della colonna 'AA-ADD - Anno di dichiarazione degli addetti' in int
for i in range(len(dataframes_list)):
    df = dataframes_list[i]
    df['AA-ADD - Anno di dichiarazione degli addetti'] = df['AA-ADD - Anno di dichiarazione degli addetti'].fillna(0)
    df['AA-ADD - Anno di dichiarazione degli addetti'] = df['AA-ADD - Anno di dichiarazione degli addetti'].astype(int)
    dataframes_list[i] = df[df["UL-SEDE - Unità Locale o sede dell'impresa"].isin(['SEDE'])]

# filtrare i dati per status "ATTIVA"
for i in range(len(dataframes_list)):
    df = dataframes_list[i]
    dataframes_list[i] = df[df["stato impresa/ul"].isin(['ATTIVA'])]

# filtrare i dati per anno
anno_riferimento = 2016
for i in range(len(dataframes_list)):
    df = dataframes_list[i]
    anno_riferimento = anno_riferimento + 1
    dataframes_list[i] =  df[df["AA-ADD - Anno di dichiarazione degli addetti"].isin([anno_riferimento])]

# rinominare i dataset con l'anno di riferimento
df_2018 = dataframes_list[0]
df_2019 = dataframes_list[1]
df_2020 = dataframes_list[2]
df_2021 = dataframes_list[3]
df_2022 = dataframes_list[4]

# unione dei dataset della lista
dframe = [df_2018, df_2019, df_2020, df_2021, df_2022]
result = pd.concat([dframe[i] for i in range(len(dframe))], axis=0)

# tabella pivot in base al numero di addetti dipendenti e indipendenti per anno
pivot_selection = result[["c fiscale", "AA-ADD - Anno di dichiarazione degli addetti", "DIP - Numero addetti dipendenti", "IND - Numero addetti indipendenti"]]
df_pivot = pivot_selection.pivot_table(index='c fiscale', columns='AA-ADD - Anno di dichiarazione degli addetti', aggfunc=sum)

#format column names
df_pivot.columns = ['_'.join(str(s).strip() for s in col if s) for col in df_pivot.columns]
df_pivot.reset_index(inplace=True)

# aggiungere altre informazioni alla tabella pivot (e.g. provincia, status,...) da df_2022 (solo imprese attive)
df_info = df_2022[["c fiscale", "PRV - Provincia", "rea", "tipo impresa"]]
df_final = pd.merge(df_pivot, df_info, how='left', left_on='c fiscale', right_on='c fiscale')

# Saving in csv format
current_path = Path(os.getcwd())
data_subdir = "output"
data_path = current_path.parent / data_subdir
data_dir = str(data_path)
final_file = data_dir + '\\' + 'addetti_storico.csv'
df_final.to_csv(    final_file, 
                    sep ='|',   
                    encoding='utf-8-sig', 
                    index=False
                    )