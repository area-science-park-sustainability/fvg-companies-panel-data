# Script 01: Panel data on regional subsidies for innovation and balance sheets of FVG enterprises

# Authors: Andrea Bincoletto, Leyla Vesnic @Area Science Park
# Description: python script to clean and prepare historical data of FVG capital companies (2019-2022)
# Source: Innovation Intelligence FVG (I2FVG)


# Setup
import sys
import os
from pathlib import Path
# %matplotlib inline
import matplotlib as plt
from matplotlib import pyplot as plt
import datetime
import pandas as pd
import numpy as np
import glob
pd.options.display.max_columns = None


# Master data and locations of capital companies in Innovation Intelligence FVG

# Directory per la cartella "input"
current_path = Path(os.getcwd())
data_subdir = "input\\finanziamenti_bilanci"
data_path = current_path / data_subdir
data_dir = str(data_path)
listafile = os.listdir(data_path)
listafile = list(filter(lambda x: 'i2fvg_' in x, listafile))
assert len(listafile) >= 1, f"Errore: non trovo i file dati nella cartella {data_dir}"

# Percorso del file
file_da_elaborare = 'i2fvg_localizzazioni_imprese'
excel_file  = data_dir + "\\" + file_da_elaborare + '.xlsx'

# Lettura file excel
df_localizzazioni = pd.read_excel(excel_file, header=0, dtype=str, keep_default_na=True)
df_localizzazioni['A-Provincia'] = df_localizzazioni['A-Provincia'].replace(np.nan, 'NA', regex=True)


# How to anonymise companies with an unique identification code
# # aggiungere la nuova colonna
# df_cf_univoco = df_localizzazioni[['A-Codice fiscale']]
# df_cf_univoco.drop_duplicates(subset='A-Codice fiscale', inplace = True)
# df_cf_univoco.reset_index(inplace = True)
# df_cf_univoco.reset_index(inplace = True)
# df_cf_univoco['id_impresa'] = df_cf_univoco['level_0'] + 1
# df_cf_univoco.drop(columns=['level_0', 'index'], inplace = True)
# cols_order = ['id_impresa', 'A-Codice fiscale']
# df_cf_univoco = df_cf_univoco[cols_order]
# df_localizzazioni = df_localizzazioni.merge(df_cf_univoco, on = 'A-Codice fiscale', how = 'inner')


# Regional subsidies for innovation (2007-2021)

# Percorso file
file_da_elaborare = 'i2fvg_finanziamentiFVG_imprese'
excel_file  = data_dir + "\\" + file_da_elaborare + '.xlsx'

# Lettura file excel
df_finanz_regionali = pd.read_excel(excel_file, header=0, dtype=str, keep_default_na=True)

# drop dei finanziamenti senza data di inizio progetto
nan_value = float('NaN')
df_finanz_regionali.replace("", nan_value, inplace = True)
df_finanz_regionali.dropna(subset=['C-Finanziamenti FVG data inizio progetto'], inplace=True)

# colonna con il riferimento dell'anno di inizio progetto
df_finanz_regionali['Anno'] = pd.DatetimeIndex(df_finanz_regionali['C-Finanziamenti FVG data inizio progetto']).year
df_finanz_regionali['Anno'] = df_finanz_regionali['Anno'].astype(int)

# Graphical display of the number of projects per year

# preparazione dei dati per il grafico
df_plot = df_finanz_regionali[['A-Codice fiscale', 'Anno']]
df_plot_linea = df_plot.pivot_table(values='A-Codice fiscale', index = 'Anno',aggfunc= 'count')
df_plot_linea.columns.name = None
df_plot_linea = df_plot_linea.reset_index()

# grafico a linee
plt.plot(df_plot_linea['Anno'], df_plot_linea['A-Codice fiscale'], marker="o", markersize=5, markeredgecolor="navy", markerfacecolor="white")
plt.xlabel("Anno", labelpad = 10)
# plt.xticks([2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021], rotation=45)
plt.xticks(df_plot_linea['Anno'], rotation=45)
plt.ylabel("Numero di imprese", labelpad = 10)
plt.title = ('Distribuzione temporale delle imprese che hanno avuto accesso a finanziamenti regionali per innovazione')
plt.grid()
for x,y in zip(df_plot_linea['Anno'], df_plot_linea['A-Codice fiscale']):
    label = "{:}".format(y)
    plt.annotate(label, # this is the text
                 (x,y), # these are the coordinates to position the label
                 textcoords="offset points", # how to position the text
                 xytext=(-1,9), # distance from text to points (x,y)
                 ha='center',
                 color='navy')
plt.margins(x=0.01, y=0.1)
plt.show


# New column with the annual sum of the number of subsidies received

# dataframe con una colonna per anno (2007-2021)
df_test = df_finanz_regionali[['A-Codice fiscale', 'Anno']]
df_pivot = df_test.pivot_table(index='A-Codice fiscale', columns='Anno', aggfunc=len, fill_value='0')

# df_pivot.columns = df_pivot.columns.droplevel(0)    #remove amount
df_pivot.columns.name = None                        #remove categories
df_pivot = df_pivot.reset_index()                   #index to columns
column_list = ['2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020','2021']
year = 2007     # to modify if first original file has changed
for i in range(len(column_list)):
    df_pivot[year] = df_pivot[year].apply(str)
    year = year + 1

# unire le informazioni sui finanziamenti regionali al dataframe principale relativo alle localizzazioni
df_localizzazioni = pd.merge(left=df_localizzazioni, right=df_pivot, on='A-Codice fiscale', how='left')
df_localizzazioni = df_localizzazioni.drop_duplicates()
year = 2007     # to modify if first original file has changed
for i in range(len(column_list)):
    df_localizzazioni[year] = df_localizzazioni[year].fillna(0)
    year = year + 1

# mantengo nel campione solamente le sedi delle localizzazioni (escludo le unità locali)
# quindi la singola impresa appare solo una volta per riga
df_loc_sedi = df_localizzazioni[df_localizzazioni['A-Sede/U.L.'].isin(['SEDE'])]
year = 2007     # da modificare con l'anno iniziale dei dati storici a disposizione
for i in range(len(column_list)):
    df_loc_sedi[year] = df_loc_sedi[year].astype(float).astype(int)
    year = year + 1

# colonna dicotomica SI/NO se l'impresa ha ricevuto finanziamenti FVG 
cols = [2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021]
df_loc_sedi['totale'] = df_loc_sedi[cols].sum(axis=1).astype(int)
def j(row):
    if row['totale'] > 0:
        val = 'SI'
    else:
        val = 'NO'
    return val
df_loc_sedi['FinanzFVG_SI/NO'] = df_loc_sedi.apply(j, axis = 1)
df_loc_sedi = df_loc_sedi.drop('totale', axis=1)

# Saving in csv format
current_path = Path(os.getcwd())
data_subdir = "output"
data_path = current_path / data_subdir
data_dir = str(data_path)

# salvataggio di tutte le localizzazioni
file_risultati = data_dir + '\\' + 'finanziamenti_localizzazioni.csv'
df_localizzazioni.to_csv(   file_risultati, 
                            sep ='|',   
                            encoding='utf-8-sig', 
                            index=False)

# salvataggio solo delle sedi
file_risultati = data_dir + '\\' + 'finanziamenti_sedi.csv'
df_loc_sedi.to_csv( file_risultati, 
                    sep ='|',   
                    encoding='utf-8-sig', 
                    index=False)


# Balance sheets (2013-2020)

# directory "input"
current_path = Path(os.getcwd())
data_subdir = "input//finanziamenti_bilanci"
data_path = current_path / data_subdir
data_dir = str(data_path)
listafile = os.listdir(data_path)
listafile = list(filter(lambda x: 'bilanci' in x, listafile))
assert len(listafile) >= 1, f"Errore: non trovo i file dati nella cartella {data_dir}"

# percorso file
file_da_elaborare = 'bilanci_mag_2022'   # nome da modificare per ogni file
excel_file  = data_dir + "\\" + file_da_elaborare + '.xlsx'
xl = pd.ExcelFile(  excel_file, engine="openpyxl")
xl_sheets = xl.sheet_names  # see all sheet names
assert xl_sheets == ['FRIULI anagrafica', 'FRIULI dati storicizzati' ,'FRIULI codici attività']
df_bilanci = xl.parse(  'FRIULI dati storicizzati', 
                        header = 0, 
                        dtype={
                            'c fiscale': str,
                            'anno': str,
                            'Totale attivo': float,
                            'Totale Immobilizzazioni immateriali': float,
                            'Crediti esigibili entro l esercizio successivo': str,
                            'Totale patrimonio netto': float,
                            'Debiti esigibili entro l esercizio successivo': str,
                            'Totale valore della produzione': float,
                            'Ricavi delle vendite': float,
                            'Totale Costi del Personale': float,
                            'Differenza tra valore e costi della produzione': float,
                            'Ammortamento Immobilizzazione Immateriali': float,
                            'Utile/perdita esercizio ultimi': float,
                            'valore aggiunto': float,
                            'tot.aam.acc.svalutazioni': float,
                            '(ron) reddito operativo netto': float
                            },
                        keep_default_na=False)
df_bilanci.rename(columns = {'c fiscale':'A-Codice fiscale'}, inplace = True)
df_loc_bil_finanz = df_loc_sedi.merge(df_bilanci, on = 'A-Codice fiscale', how = 'left')
df_loc_bil = df_loc_bil_finanz[['A-Codice fiscale','anno', 'Totale attivo',
       'Totale Immobilizzazioni immateriali',
       "Crediti esigibili entro l esercizio successivo",
       'Totale patrimonio netto',
       "Debiti esigibili entro l esercizio successivo",
       'Totale valore della produzione', 'Ricavi delle vendite',
       'Totale Costi del Personale',
       'Differenza tra valore e costi della produzione',
       'Ammortamento Immobilizzazione Immateriali',
       'Utile/perdita esercizio ultimi', 'valore aggiunto',
       'tot.aam.acc.svalutazioni', '(ron) reddito operativo netto']]

# Saving in csv format
current_path = Path(os.getcwd())
data_subdir = "output"
data_path = current_path / data_subdir
data_dir = str(data_path)
file_risultati = data_dir + '\\' + 'bilanci_storico.csv'
df_loc_bil.to_csv(  file_risultati, 
                    sep ='|',   
                    encoding='utf-8-sig', 
                    index=False)